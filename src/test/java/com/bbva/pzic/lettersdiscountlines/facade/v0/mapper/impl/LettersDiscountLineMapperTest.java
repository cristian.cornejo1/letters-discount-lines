package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.lettersdiscountlines.EntityMock;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.ILettersDiscountLineMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class LettersDiscountLineMapperTest {

    private ILettersDiscountLineMapper mapper;

    @Before
    public void setUp() {
        mapper = new LettersDiscountLineMapper();
    }
    @Test
    public void mapInFullTest() {

        InputBusinessId result = mapper.mapIn(EntityMock.ID);
        assertNotNull(result);
        assertNotNull(result.getBusinessId());
        assertEquals(EntityMock.ID,result.getBusinessId());
    }

    @Test
    public void mapInEmptyTest(){
        InputBusinessId result = mapper.mapIn(null);
        assertNotNull(result);
        assertNull(result.getBusinessId());
    }

    @Test
    public void mapOutFullTest(){
        List<DTOHistoricalLettersSummarie> input = EntityMock.getInstance().getLettersDiscountLines();
        ServiceResponseOK<List<HistoricalLettersSummarie>> result = mapper
                .mapOut(input);
        assertNotNull(result);
        assertNotNull(result.getData());
        assertNotNull(result.getData());
        assertNotNull(result.getData().get(0));
        assertNotNull(result.getData().get(0).getId());
        assertNotNull(result.getData().get(0).getName());
        assertNotNull(result.getData().get(0).getHistoricalParameters());
        assertNotNull(result.getData().get(0).getHistoricalParameters().get(0));
        assertNotNull(result.getData().get(0).getHistoricalParameters().get(0).getPercentage());
        assertNotNull(result.getData().get(0).getHistoricalParameters().get(0).getInformationPeriod());
        assertNotNull(result.getData().get(0).getHistoricalParameters().get(0).getInformationPeriod().getUnit());
        assertNotNull(result.getData().get(0).getHistoricalParameters().get(0).getInformationPeriod().getNumber());
        assertEquals(EntityMock.ID,result.getData().get(0).getId());
        assertEquals(EntityMock.NAME,result.getData().get(0).getName());
        assertEquals(EntityMock.NUMBER,result.getData().get(0).getHistoricalParameters().get(0).getInformationPeriod().getNumber());
        assertEquals(EntityMock.UNIT,result.getData().get(0).getHistoricalParameters().get(0).getInformationPeriod().getUnit());
        assertEquals(EntityMock.PERCENTAGE,result.getData().get(0).getHistoricalParameters().get(0).getPercentage());
    }

    @Test
    public void mapOutEmptyTest(){

        ServiceResponseOK<List<HistoricalLettersSummarie>> result = mapper
                .mapOut(null);
        assertNull(result);
    }
}