package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.impl;

import com.bbva.pzic.lettersdiscountlines.EntityMock;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.mock.FormatoRIF4Mock;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.ITXListHistoricalLettersSummariesMapper;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
/**
 * Created on 20/04/2021.
 *
 * @author Entelgy.
 */

public class TXListHistoricalLettersSummariesMapperTest  {

   private ITXListHistoricalLettersSummariesMapper mapper;
   private static final String ID = "EFECTIVIDAD_LETRAS_SOLES";
   private static final String EFEDES = "EFECTIVIDAD DE LETRAS EN SOLES";
   private static final Integer PEREFEL = 6;
   private static final String UNIEFEL = "MONTHS";
   private static final Integer POREFE = 44;

   @Before
    public void setUp(){
       mapper = new TXListHistoricalLettersSummariesMapper();
   }

   @Test
    public void mapInFullTest(){
       InputBusinessId input = EntityMock.getInstance().getInputBusinessId();
       FormatoRIMRF40 result = mapper.mapIn(input);
       assertNotNull(result);
       assertNotNull(result.getCodcent());
       assertEquals(input.getBusinessId(),result.getCodcent());
   }

   @Test
   public void mapOnFullTestFormatoRIMRF41(){
      FormatoRIMRF41 input = FormatoRIF4Mock.getInstance().getFormatoRIMRF41();
      List<DTOHistoricalLettersSummarie> historicalLettersSumaries = new ArrayList<>();
      List<DTOHistoricalLettersSummarie> result = mapper.mapOut(input,historicalLettersSumaries);

      assertNotNull(result);
      assertNotNull(result);
      assertNotNull(result.get(0));
      assertNotNull(result.get(0).getId());
      assertNotNull(result.get(0).getName());
      assertEquals(ID,result.get(0).getId());
      assertEquals(EFEDES,result.get(0).getName());
   }

   @Test
   public void mapOnFullTestFormatoRIMRF42(){
      FormatoRIMRF42 input = FormatoRIF4Mock.getInstance().getFormatoRIMRF42();

      List<DTOHistoricalLettersSummarie> historicalLettersSumaries = new ArrayList<>();
      historicalLettersSumaries.add(new DTOHistoricalLettersSummarie());
      List<DTOHistoricalLettersSummarie> result = mapper.mapOut2(input,historicalLettersSumaries);

      assertNotNull(result);
      assertNotNull(result);
      assertNotNull(result.get(0));
      assertNull(result.get(0).getId());
      assertNull(result.get(0).getName());
      assertNotNull(result.get(0).getHistoricalParameters());
      assertNotNull(result.get(0).getHistoricalParameters().get(0).getPercentage());
      assertNotNull(result.get(0).getHistoricalParameters().get(0).getInformationPeriod());
      assertNotNull(result.get(0).getHistoricalParameters().get(0).getInformationPeriod().getNumber());
      assertNotNull(result.get(0).getHistoricalParameters().get(0).getInformationPeriod().getUnit());
      assertEquals(PEREFEL,result.get(0).getHistoricalParameters().get(0).getInformationPeriod().getNumber());
      assertEquals(UNIEFEL,result.get(0).getHistoricalParameters().get(0).getInformationPeriod().getUnit());
      assertEquals(POREFE,result.get(0).getHistoricalParameters().get(0).getPercentage());
   }
}