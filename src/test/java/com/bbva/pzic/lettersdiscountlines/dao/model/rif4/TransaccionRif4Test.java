package com.bbva.pzic.lettersdiscountlines.dao.model.rif4;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TransaccionRif4Test {

    @InjectMocks
    private TransaccionRif4 transaccion;

    @Mock
    private ServicioTransacciones servicioTransacciones;

    @Test
    public void test() throws ExcepcionTransaccion{

        PeticionTransaccionRif4 peticion = new PeticionTransaccionRif4();

        RespuestaTransaccionRif4 respuesta = transaccion.invocar(peticion);

        when(servicioTransacciones.invocar(
                PeticionTransaccionRif4.class,
                RespuestaTransaccionRif4.class,
                peticion))
                .thenReturn(respuesta);

        RespuestaTransaccionRif4 result = transaccion.invocar(peticion);

        assertEquals(result, respuesta);

    }

}