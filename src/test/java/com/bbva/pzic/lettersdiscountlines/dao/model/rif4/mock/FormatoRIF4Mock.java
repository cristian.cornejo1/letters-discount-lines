package com.bbva.pzic.lettersdiscountlines.dao.model.rif4.mock;

import com.bbva.jee.arq.spring.core.servicing.gce.BusinessServiceException;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.util.mappers.ObjectMapperHelper;

import java.io.IOException;

/**
 * Created on 20/04/2021.
 *
 * @author Entelgy
 */
public class FormatoRIF4Mock {
    private final ObjectMapperHelper mapper = ObjectMapperHelper.getInstance();
    private static final FormatoRIF4Mock INSTANCE = new FormatoRIF4Mock();
    public static final String TECHNICAL_ERROR = "technicalError";
    private FormatoRIF4Mock() {
    }
    public static FormatoRIF4Mock getInstance() {
        return INSTANCE;
    }

    public FormatoRIMRF41 getFormatoRIMRF41() {
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "com/bbva/pzic/lettersdiscountlines/dao/model/rif4/mock/formatoRIMRF41.json"),
                    FormatoRIMRF41.class);
        } catch (IOException e) {
            throw new BusinessServiceException(TECHNICAL_ERROR, e);
        }
    }

    public FormatoRIMRF42 getFormatoRIMRF42(){
        try {
            return mapper.readValue(Thread.currentThread().getContextClassLoader().getResourceAsStream(
                    "com/bbva/pzic/lettersdiscountlines/dao/model/rif4/mock/formatoRIMRF42.json"),
                    FormatoRIMRF42.class);
        } catch (IOException e) {
            throw new BusinessServiceException(TECHNICAL_ERROR, e);
        }
    }
}
