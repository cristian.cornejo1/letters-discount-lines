package com.bbva.pzic.lettersdiscountlines;

import com.bbva.pzic.lettersdiscountlines.business.dto.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Entelgy
 */
public class EntityMock {

    private static final EntityMock INSTANCE = new EntityMock();
    public static final String ID = "EFECTIVIDAD_LETRAS_SOLES";
    public static final String NAME = "EFECTIVIDAD DE LETRAS EN SOLES";
    public static final Integer NUMBER = 6;
    public static final String UNIT = "MONTHS";
    public static final Integer PERCENTAGE = 44;


    private EntityMock() {
    }

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public InputBusinessId getInputBusinessId(){
        InputBusinessId input = new InputBusinessId();

        input.setBusinessId(ID);
        return input;
    }

    public List<DTOHistoricalLettersSummarie> getLettersDiscountLines(){
        List<DTOHistoricalLettersSummarie> historicalLettersSummaries = new ArrayList<>();

        DTOHistoricalLettersSummarie historicalLettersSummarie = new DTOHistoricalLettersSummarie();
        historicalLettersSummarie.setName(NAME);
        historicalLettersSummarie.setId(ID);
        DTOHistoricalParameter historicalParameter = new DTOHistoricalParameter();
        historicalParameter.setPercentage(PERCENTAGE);
        DTOInformationPeriod informationPeriod = new DTOInformationPeriod();

        informationPeriod.setNumber(NUMBER);
        informationPeriod.setUnit(UNIT);

        historicalParameter.setInformationPeriod(informationPeriod);

        List<DTOHistoricalParameter> historicalParameters = new ArrayList<>();
        historicalParameters.add(historicalParameter);

        historicalLettersSummarie.setHistoricalParameters(historicalParameters);
        historicalLettersSummaries.add(historicalLettersSummarie);
        return historicalLettersSummaries;
    }

}
