package com.bbva.pzic.lettersdiscountlines.util.converter;

import org.springframework.util.CollectionUtils;

import java.util.List;

import static org.apache.commons.lang.StringUtils.isEmpty;

public final class UtilsConverter {

    private UtilsConverter() {
    }

    public static boolean isAllEmpty(Object... values) {
        for (Object value : values) {
            if (value instanceof String) {
                if (!isEmpty((String) value)) return false;
            } else if (value instanceof List) {
                if (!CollectionUtils.isEmpty((List) value)) return false;
            } else if (value != null) {
                return false;
            }
        }
        return true;
    }
}