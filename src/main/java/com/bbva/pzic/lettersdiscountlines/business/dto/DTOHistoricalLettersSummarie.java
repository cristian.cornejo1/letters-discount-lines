package com.bbva.pzic.lettersdiscountlines.business.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy
 */
public class DTOHistoricalLettersSummarie {

    @Size(max = 26, groups = ValidationGroup.LettersDiscountLine.class)
    @NotNull(groups = ValidationGroup.LettersDiscountLine.class)
    private String id;

    @Size(max = 32, groups = ValidationGroup.LettersDiscountLine.class)
    @NotNull(groups = ValidationGroup.LettersDiscountLine.class)
    private String name;

    private List<DTOHistoricalParameter> historicalParameters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DTOHistoricalParameter> getHistoricalParameters() {
        return historicalParameters;
    }

    public void setHistoricalParameters(List<DTOHistoricalParameter> historicalParameters) {
        this.historicalParameters = historicalParameters;
    }
}
