package com.bbva.pzic.lettersdiscountlines.business.impl;

import com.bbva.pzic.lettersdiscountlines.business.ISrvIntLettersDiscountLines;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 01/10/2018.
 *
 * @author Entelgy
 */
@Component
public class SvrIntLettersDiscountLines implements ISrvIntLettersDiscountLines {


    private static final Log LOG = LogFactory.getLog(SvrIntLettersDiscountLines.class);

    @Autowired
    private Validator validator;

    @Autowired
    private ILettersDiscountLinesDAO lettersDiscountLinesDAO;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DTOHistoricalLettersSummarie> listHistoricalLettersSummaries(final InputBusinessId inputBusinessId) {
        LOG.info("... Invoking method SvrIntLettersDiscountLines.listHistoricalLettersSummaries ...");
        validator.validate(inputBusinessId);
        return lettersDiscountLinesDAO.listHistoricalLettersSummaries(inputBusinessId);
    }
}
