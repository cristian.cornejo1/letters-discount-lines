package com.bbva.pzic.lettersdiscountlines.business.dto;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy
 */
public class DTOHistoricalParameter {


    private DTOInformationPeriod informationPeriod;

    private Integer percentage;

    public DTOInformationPeriod getInformationPeriod() {
        return informationPeriod;
    }

    public void setInformationPeriod(DTOInformationPeriod informationPeriod) {
        this.informationPeriod = informationPeriod;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
