package com.bbva.pzic.lettersdiscountlines.business.dto;

import com.bbva.jee.arq.spring.core.auditoria.DatoAuditable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy
 */
public class InputBusinessId {

    @DatoAuditable(omitir = true)
    @Size(max = 8, groups = ValidationGroup.LettersDiscountLine.class)
    @NotNull(groups = ValidationGroup.LettersDiscountLine.class)
    private String businessId;

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }
}
