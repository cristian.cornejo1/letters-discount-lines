package com.bbva.pzic.lettersdiscountlines.business.dto;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy
 */
public class DTOInformationPeriod {
    private Integer number;
    private String unit;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
