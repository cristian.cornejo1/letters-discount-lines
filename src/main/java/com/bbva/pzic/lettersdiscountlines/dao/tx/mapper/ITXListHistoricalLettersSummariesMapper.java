package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;

import java.util.List;

public interface ITXListHistoricalLettersSummariesMapper {
    FormatoRIMRF40 mapIn(InputBusinessId inputBusinessId);
    List<DTOHistoricalLettersSummarie> mapOut(FormatoRIMRF41 formatOutput, List<DTOHistoricalLettersSummarie> historicalLettersSummaries);
    List<DTOHistoricalLettersSummarie> mapOut2(FormatoRIMRF42 formatOutput, List<DTOHistoricalLettersSummarie> historicalLettersSummaries);
}
