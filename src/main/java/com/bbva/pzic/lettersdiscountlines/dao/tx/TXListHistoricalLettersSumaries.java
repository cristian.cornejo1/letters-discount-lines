package com.bbva.pzic.lettersdiscountlines.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.*;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.impl.TXListHistoricalLettersSummariesMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.DoubleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy.
 */
@Component
public class TXListHistoricalLettersSumaries extends DoubleOutputFormat<InputBusinessId, FormatoRIMRF40, List<DTOHistoricalLettersSummarie>, FormatoRIMRF41, FormatoRIMRF42> {

    @Autowired
    private TXListHistoricalLettersSummariesMapper mapper;

    public TXListHistoricalLettersSumaries(@Qualifier("transaccionRif4")InvocadorTransaccion<PeticionTransaccionRif4, RespuestaTransaccionRif4> transaction) {
        super(transaction, PeticionTransaccionRif4::new, ArrayList::new, FormatoRIMRF41.class, FormatoRIMRF42.class);
    }

    @Override
    protected FormatoRIMRF40 mapInput(InputBusinessId inputBusinessId) {
        return mapper.mapIn(inputBusinessId);
    }

    @Override
    protected  List<DTOHistoricalLettersSummarie> mapFirstOutputFormat(FormatoRIMRF41 formatoRIMRF41, InputBusinessId inputBusinessId,  List<DTOHistoricalLettersSummarie> historicalLettersSummaries) {
        return mapper.mapOut(formatoRIMRF41,historicalLettersSummaries);
    }

    @Override
    protected  List<DTOHistoricalLettersSummarie> mapSecondOutputFormat(FormatoRIMRF42 formatoRIMRF42, InputBusinessId inputBusinessId,  List<DTOHistoricalLettersSummarie> historicalLettersSummaries) {
        return mapper.mapOut2(formatoRIMRF42,historicalLettersSummaries);
    }
}
