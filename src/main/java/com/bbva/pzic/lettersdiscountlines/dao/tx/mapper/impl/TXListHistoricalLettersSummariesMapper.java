package com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.impl;

import com.bbva.pzic.lettersdiscountlines.business.dto.*;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF40;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF41;
import com.bbva.pzic.lettersdiscountlines.dao.model.rif4.FormatoRIMRF42;
import com.bbva.pzic.lettersdiscountlines.dao.tx.mapper.ITXListHistoricalLettersSummariesMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import static com.bbva.pzic.lettersdiscountlines.util.converter.UtilsConverter.isAllEmpty;

@Component
public class TXListHistoricalLettersSummariesMapper implements ITXListHistoricalLettersSummariesMapper {


    private static final Log LOG = LogFactory.getLog(TXListHistoricalLettersSummariesMapper.class);

    @Override
    public FormatoRIMRF40 mapIn(final InputBusinessId inputBusinessId) {
        LOG.info("... called method TXListHistoricalLettersSummariesMapper.mapIn ...");
        FormatoRIMRF40 formato = new FormatoRIMRF40();
        formato.setCodcent(inputBusinessId.getBusinessId());
        return formato;
    }

    @Override
    public  List<DTOHistoricalLettersSummarie> mapOut(final FormatoRIMRF41 formatOutput,List<DTOHistoricalLettersSummarie> historicalLettersSummaries) {
        LOG.info("... called method TXListHistoricalLettersSummariesMapper.mapOut ...");

        DTOHistoricalLettersSummarie historicalLettersSumaries = new DTOHistoricalLettersSummarie();
        historicalLettersSumaries.setId(formatOutput.getId() == null ? null : formatOutput.getId());
        historicalLettersSumaries.setName(formatOutput.getEfedes() == null ? null : formatOutput.getEfedes());
        historicalLettersSummaries.add(historicalLettersSumaries);
        return historicalLettersSummaries;
    }


    @Override
    public  List<DTOHistoricalLettersSummarie> mapOut2(final FormatoRIMRF42 formatOutput, final List<DTOHistoricalLettersSummarie> historicalLettersSummaries) {
        LOG.info("... called method TXListHistoricalLettersSummariesMapper.mapOut2 ...");
        if (isAllEmpty(formatOutput.getPerefel(), formatOutput.getUniefel(), formatOutput.getPorefe())) {
            return historicalLettersSummaries;
        }

        DTOHistoricalLettersSummarie historicalLettersSumaries = historicalLettersSummaries.get(historicalLettersSummaries.size() - 1);
        if (CollectionUtils.isEmpty(historicalLettersSumaries.getHistoricalParameters())) {
            historicalLettersSumaries.setHistoricalParameters(new ArrayList<>());
        }
        DTOHistoricalParameter historicalParameters = new DTOHistoricalParameter();
        historicalParameters.setInformationPeriod(mapOutInformationPeriod(formatOutput.getPerefel(), formatOutput.getUniefel()));
        historicalParameters.setPercentage(formatOutput.getPorefe());
        historicalLettersSumaries.getHistoricalParameters().add(historicalParameters);
        return historicalLettersSummaries;
    }

    private DTOInformationPeriod mapOutInformationPeriod(final Integer perefel, String uniefel) {
        if (perefel == null && uniefel == null) {
            return null;
        }
        DTOInformationPeriod informationPeriod = new DTOInformationPeriod();
        informationPeriod.setNumber(perefel);
        informationPeriod.setUnit(uniefel);
        return informationPeriod;
    }
}
