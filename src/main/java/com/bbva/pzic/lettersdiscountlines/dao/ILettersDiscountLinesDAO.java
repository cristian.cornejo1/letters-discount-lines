package com.bbva.pzic.lettersdiscountlines.dao;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;

import java.util.List;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy
 */
public interface ILettersDiscountLinesDAO {
    List<DTOHistoricalLettersSummarie> listHistoricalLettersSummaries(InputBusinessId businessId);
}
