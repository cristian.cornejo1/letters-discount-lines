package com.bbva.pzic.lettersdiscountlines.dao.impl;

import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.dao.ILettersDiscountLinesDAO;
import com.bbva.pzic.lettersdiscountlines.dao.tx.TXListHistoricalLettersSumaries;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created on 19/04/2021.
 *
 * @author Entelgy
 */
@Component
public class LettersDiscountLinesDAO implements ILettersDiscountLinesDAO {


    private static final Log LOG = LogFactory.getLog(LettersDiscountLinesDAO.class);

    @Autowired
    private TXListHistoricalLettersSumaries txListHistoricalLettersSumaries;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<DTOHistoricalLettersSummarie> listHistoricalLettersSummaries(final InputBusinessId businessId) {
        LOG.info("... Invoking method LettersDiscountLinesDAO.listHistoricalLettersSummaries ...");
        return txListHistoricalLettersSumaries.perform(businessId);
    }
}
