package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.lettersdiscountlines.business.dto.*;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalParameter;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.InformationPeriod;
import com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.ILettersDiscountLineMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class LettersDiscountLineMapper implements ILettersDiscountLineMapper {

    private static final Log LOG = LogFactory.getLog(LettersDiscountLineMapper.class);

    @Override
    public InputBusinessId mapIn(String businessId) {
        LOG.info("... called method LettersDiscountLineMapper.mapIn ...");
        InputBusinessId input = new InputBusinessId();
        input.setBusinessId(businessId);
        return input;
    }


    @Override
    public ServiceResponseOK<List<HistoricalLettersSummarie>> mapOut(final List<DTOHistoricalLettersSummarie> historicalLettersSummaries) {
        LOG.info("... called method LettersDiscountLineMapper.mapOut ...");
        if(CollectionUtils.isEmpty(historicalLettersSummaries)){
            return null;
        }
        return ServiceResponseOK.data(historicalLettersSummaries.stream().filter(Objects::nonNull).map(this::mapOutHistoricalLettersSummarie).collect(Collectors.toList())).pagination(null).build();
    }

    private HistoricalLettersSummarie mapOutHistoricalLettersSummarie(final DTOHistoricalLettersSummarie dtoHistoricalLettersSummarie) {

        if(dtoHistoricalLettersSummarie == null){
            return null;
        }
        HistoricalLettersSummarie historicalLettersSummarie = new HistoricalLettersSummarie();
        historicalLettersSummarie.setHistoricalParameters(mapOutHistoricalParameters(dtoHistoricalLettersSummarie.getHistoricalParameters()));
        historicalLettersSummarie.setId(dtoHistoricalLettersSummarie.getId());
        historicalLettersSummarie.setName(dtoHistoricalLettersSummarie.getName());
        return  historicalLettersSummarie;
    }

    private List<HistoricalParameter> mapOutHistoricalParameters(final List<DTOHistoricalParameter> historicalParameters) {
        if(CollectionUtils.isEmpty(historicalParameters)){
            return null;
        }
        return historicalParameters.stream().filter(Objects::nonNull).map(this::mapOutHistoricalParameter).collect(Collectors.toList());
    }

    private HistoricalParameter mapOutHistoricalParameter(final DTOHistoricalParameter dtoHistoricalParameter) {

        if(dtoHistoricalParameter == null){
            return null;
        }

        HistoricalParameter historicalParameter = new HistoricalParameter();
        historicalParameter.setInformationPeriod(mapOutInformationPeriod(dtoHistoricalParameter.getInformationPeriod()));
        historicalParameter.setPercentage(dtoHistoricalParameter.getPercentage());
        return historicalParameter;
    }

    private InformationPeriod mapOutInformationPeriod(final DTOInformationPeriod dtoInformationPeriod) {
        if(dtoInformationPeriod == null){
            return null;
        }
        InformationPeriod informationPeriod = new InformationPeriod();
        informationPeriod.setNumber(dtoInformationPeriod.getNumber());
        informationPeriod.setUnit(dtoInformationPeriod.getUnit());
        return informationPeriod;
    }


}
