package com.bbva.pzic.lettersdiscountlines.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.lettersdiscountlines.business.dto.DTOHistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.business.dto.InputBusinessId;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummarie;

import java.util.List;

public interface ILettersDiscountLineMapper {

    InputBusinessId mapIn(String businessId);
    ServiceResponseOK<List<HistoricalLettersSummarie>> mapOut(List<DTOHistoricalLettersSummarie> historicalLettersSummaries);
}
