package com.bbva.pzic.lettersdiscountlines.facade.v0.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.jee.arq.spring.core.servicing.annotations.SMC;
import com.bbva.jee.arq.spring.core.servicing.annotations.SN;
import com.bbva.jee.arq.spring.core.servicing.annotations.VN;
import com.bbva.pzic.lettersdiscountlines.business.ISrvIntLettersDiscountLines;
import com.bbva.pzic.lettersdiscountlines.facade.v0.ISrvLettersDiscountLinesV0;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummarie;
import com.bbva.pzic.lettersdiscountlines.facade.v0.mapper.impl.LettersDiscountLineMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import java.util.List;

import static com.bbva.pzic.lettersdiscountlines.facade.RegistryIds.SN_REGISTRY_ID_LETTERS;

/**
 * Created on 19/04/2021
 *
 * @author Entelgy
 */

@Path("/v0")
@Produces(MediaType.APPLICATION_JSON)
@SN(registryID = SN_REGISTRY_ID_LETTERS, logicalID = "letters-discount-lines")
@VN(vnn = "v0")
@Service
public class SrvLettersDiscountLinesV0 implements ISrvLettersDiscountLinesV0 {

    private static final Log LOG = LogFactory.getLog(SrvLettersDiscountLinesV0.class);
    @Autowired
    private LettersDiscountLineMapper lettersDiscountLineMapper;

    @Autowired
    private ISrvIntLettersDiscountLines srvIntLettersDiscountLines;

    /**
     * {@inheritDoc}
     */
    @Override
    @GET
    @Path("/historical-letters-summaries")
    @SMC(registryID = "SMGG20203815", logicalID = "listHistoricalLettersSummaries")
    public ServiceResponseOK<List<HistoricalLettersSummarie>> listHistoricalLettersSummaries(
            @QueryParam("businessId") final String businessId) {
        LOG.info("----- Invoking service listHistoricalLettersSummaries -----");

        return lettersDiscountLineMapper.mapOut(
                srvIntLettersDiscountLines.listHistoricalLettersSummaries(
                        lettersDiscountLineMapper.mapIn(businessId)
                )
        );
    }
}
