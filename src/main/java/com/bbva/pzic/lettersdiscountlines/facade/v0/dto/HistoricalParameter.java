package com.bbva.pzic.lettersdiscountlines.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 11/03/2021.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "historicalParameter", namespace = "urn:com:bbva:pzic:letters:facade:v0:dto")
@XmlType(name = "historicalParameter", namespace = "urn:com:bbva:pzic:letters:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoricalParameter implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * List of information period.
     */
    private InformationPeriod informationPeriod;

    /**
     * Value associated with the historical parameter represented in percentage. For example: 44.90.
     */
    private Integer percentage;

    public InformationPeriod getInformationPeriod() {
        return informationPeriod;
    }

    public void setInformationPeriod(InformationPeriod informationPeriod) {
        this.informationPeriod = informationPeriod;
    }

    public Integer getPercentage() {
        return percentage;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }
}
