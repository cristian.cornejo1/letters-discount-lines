package com.bbva.pzic.lettersdiscountlines.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * Created on 11/03/2021.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "informationPeriod", namespace = "urn:com:bbva:pzic:letters:facade:v0:dto")
@XmlType(name = "informationPeriod", namespace = "urn:com:bbva:pzic:letters:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class InformationPeriod implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     *  Value associated with the information period, where it is represented a period of time.
     */
    private Integer number;

    /**
     * Value associated with the information period, where it is represented a unit of time.
     */
    private String unit;

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
