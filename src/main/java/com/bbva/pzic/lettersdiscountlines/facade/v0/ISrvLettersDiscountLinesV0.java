package com.bbva.pzic.lettersdiscountlines.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponseOK;
import com.bbva.pzic.lettersdiscountlines.facade.v0.dto.HistoricalLettersSummarie;

import java.util.List;

public interface ISrvLettersDiscountLinesV0 {

    ServiceResponseOK<List<HistoricalLettersSummarie>> listHistoricalLettersSummaries(String businessId);
}
