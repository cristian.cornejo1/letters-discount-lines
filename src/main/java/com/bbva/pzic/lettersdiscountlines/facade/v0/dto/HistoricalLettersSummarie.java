package com.bbva.pzic.lettersdiscountlines.facade.v0.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * Created on 11/03/2021.
 *
 * @author Entelgy
 */
@XmlRootElement(name = "historicalLettersSummarie", namespace = "urn:com:bbva:pzic:letters:facade:v0:dto")
@XmlType(name = "historicalLettersSummarie", namespace = "urn:com:bbva:pzic:letters:facade:v0:dto")
@XmlAccessorType(XmlAccessType.FIELD)
public class HistoricalLettersSummarie implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Identifier associated with the historical summary.
     */
    private String id;

    /**
     * Name associated with the historical summary.
     */
    private String name;

    /**
     * List of values associated with the historical summary.
     */
    private List<HistoricalParameter> historicalParameters;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<HistoricalParameter> getHistoricalParameters() {
        return historicalParameters;
    }

    public void setHistoricalParameters(List<HistoricalParameter> historicalParameters) {
        this.historicalParameters = historicalParameters;
    }
}
